; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define Major
#define Minor
#define Build
#define Dummy
#define ApplicationName "FFS2Play64"
#define BaseDir  ".\"
#define ApplicationExeName "ffs2play.exe"
#define ApplicationFullPath BaseDir + ApplicationExeName
#expr ParseVersion(ApplicationFullPath, Major, Minor,Build,Dummy)
#define ApplicationVersion Str(Major) + "." + Str(Minor) + "." + Str(Build)
#define ApplicationPublisher "FFS2"
#define ApplicationURL "http://ffs2play.fr/"
#define ApplicationExeName "ffs2play.exe"
#define InstallFileName "ffs2play64_" + str(Major) +"_" + str(Minor) + "_" + str(Build) + "_win"



[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
ArchitecturesInstallIn64BitMode=x64
AppId={{7E043FC6-3D67-46C1-BB94-AC9C9E278E3D}}
AppName={#ApplicationName}
AppVerName={#ApplicationName} {#ApplicationVersion}
AppPublisher={#ApplicationPublisher}
AppPublisherURL={#ApplicationURL}
AppSupportURL={#ApplicationURL}
AppUpdatesURL={#ApplicationURL}
DefaultDirName={pf}\{#ApplicationName}
DisableProgramGroupPage=yes
LicenseFile=LICENSE.GPLv3
InfoBeforeFile=changelog.txt
OutputDir=.\
OutputBaseFilename= {#InstallFileName}
SetupIconFile=ffs2play_48x48.ico
Compression=lzma
SolidCompression=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "french"; MessagesFile: "compiler:Languages\French.isl"

[Components]
Name: "program"; Description: "Main Program"
[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 0,6.1

[Files]
source: "{#BaseDir}vc_redist.x64.exe"; DestDir: "{tmp}"; Flags: nocompression createallsubdirs recursesubdirs deleteafterinstall
Source: "{#ApplicationFullPath}"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}libcrypto-1_1-x64.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}libssl-1_1-x64.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}Qt5Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}Qt5Gui.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}Qt5Network.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}Qt5Sql.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}Qt5Svg.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}Qt5Widgets.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}Qt5Xml.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}ffs2play.qss"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#BaseDir}translations\*"; DestDir: "{app}\translations"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#BaseDir}platforms\*"; DestDir: "{app}\platforms"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#BaseDir}sqldrivers\*"; DestDir: "{app}\sqldrivers"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#BaseDir}iconengines\*"; DestDir: "{app}\iconengines"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#BaseDir}styles\*"; DestDir: "{app}\styles"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#BaseDir}imageformats\*"; DestDir: "{app}\imageformats"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#BaseDir}bearer\*"; DestDir: "{app}\bearer"; Flags: ignoreversion recursesubdirs createallsubdirs

; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{commonprograms}\{#ApplicationName}"; Filename: "{app}\{#ApplicationExeName}"
Name: "{commondesktop}\{#ApplicationName}"; Filename: "{app}\{#ApplicationExeName}"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#ApplicationName}"; Filename: "{app}\{#ApplicationExeName}"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\{#ApplicationExeName}"; Description: "{cm:LaunchProgram,{#StringChange(ApplicationName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
Filename: "{tmp}\vc_redist.x64.exe"; Parameters: "/Q"; Flags: waituntilterminated skipifdoesntexist; StatusMsg: "Microsoft Visual C++ 2017 (x64) installation. Please Wait..."

