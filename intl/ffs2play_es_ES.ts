<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AIBaseParser</name>
    <message>
        <location filename="../disk/aibaseparser.cpp" line="225"/>
        <source>Start updating aircraft database...</source>
        <translation>Comenzar a actualizar la base de datos de aeronaves ...</translation>
    </message>
    <message>
        <location filename="../disk/aibaseparser.cpp" line="258"/>
        <source>End of update : </source>
        <translation>Fin de la actualización : </translation>
    </message>
    <message>
        <location filename="../disk/aibaseparser.cpp" line="259"/>
        <source> aircrafts added, </source>
        <translation> aviones añadidos, </translation>
    </message>
    <message>
        <location filename="../disk/aibaseparser.cpp" line="260"/>
        <source> updated and </source>
        <translation> actualizado y </translation>
    </message>
    <message>
        <location filename="../disk/aibaseparser.cpp" line="260"/>
        <source> deleted.</source>
        <translation> eliminado.</translation>
    </message>
</context>
<context>
    <name>AIReplace</name>
    <message>
        <location filename="../gui/aireplace.ui" line="14"/>
        <source>Aircraft Replacement</source>
        <translation>Reemplazo de aviones</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="34"/>
        <source>Origin Aircraft</source>
        <translation>Aviones de origen</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="65"/>
        <source>Replacement Aircraft</source>
        <translation>Aviones de reemplazo</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="95"/>
        <source>Filters</source>
        <translation>Filtros</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="107"/>
        <source>Editor :</source>
        <translation>Editor :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="133"/>
        <source>Number of engines :</source>
        <translation>Numero de motores :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="146"/>
        <source>Model :</source>
        <translation>Modelo :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="159"/>
        <source>Engine type :</source>
        <translation>Tipo de motor :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="172"/>
        <source>Category :</source>
        <translation>Categoría :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="195"/>
        <source>Type :</source>
        <translation>Tipo :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="277"/>
        <source>Rules</source>
        <translation>Reglas</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="296"/>
        <source>Source</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="301"/>
        <source>Replacement</source>
        <translation>Reemplazo</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="314"/>
        <source>Delete Rule</source>
        <translation>Eliminar regla</translation>
    </message>
</context>
<context>
    <name>CFGManager</name>
    <message>
        <location filename="../sim/fgmanager.cpp" line="51"/>
        <source>FlightGear Module loaded</source>
        <translation>Módulo FlightGear cargado</translation>
    </message>
    <message>
        <location filename="../sim/fgmanager.cpp" line="61"/>
        <source>FlightGear Module unloaded</source>
        <translation>Módulo FlightGear descargado</translation>
    </message>
    <message>
        <location filename="../sim/fgmanager.cpp" line="71"/>
        <source>Connecting to FlightGear ...</source>
        <translation>Conectando a FlightGear ...</translation>
    </message>
    <message>
        <location filename="../sim/fgmanager.cpp" line="77"/>
        <source>Connected to FlightGear.</source>
        <translation>Conectado a FlightGear.</translation>
    </message>
    <message>
        <location filename="../sim/fgmanager.cpp" line="94"/>
        <source>Disconnected from FlightGear.</source>
        <translation>Desconectado de FlightGear.</translation>
    </message>
</context>
<context>
    <name>CP2PManager</name>
    <message>
        <location filename="../net/p2pmanager.cpp" line="273"/>
        <source>Adding UDP port mapping failed with code </source>
        <translation>La adición del mapeo de puertos UDP falló con el código </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="288"/>
        <source>Adding TCP port mappping failed with code </source>
        <translation>La adición de asignación de puerto TCP falló con el código </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="293"/>
        <source>No valid Internet Gateway Device found.</source>
        <translation>No se encontró un dispositivo de puerta de enlace de Internet válido.</translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="299"/>
        <source>No IGD UPNP Device found on the network !</source>
        <translation>¡No se ha encontrado ningún dispositivo IGD UPNP en la red!</translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="314"/>
        <source>Delete UDP port mapping failed with code </source>
        <translation>Error al eliminar la asignación de puertos UDP con el código </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="324"/>
        <source>Delete TCP port mapping failed with code </source>
        <translation>Error al eliminar la asignación de puertos TCP con el código </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="422"/>
        <location filename="../net/p2pmanager.cpp" line="464"/>
        <source>Bad IP Address</source>
        <translation>Dirección IP incorrecta</translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="450"/>
        <source>User </source>
        <translation>Usuario </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="837"/>
        <source>Drop rate : </source>
        <translation>Tasa de caída : </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="837"/>
        <source> ft/min</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CPeer</name>
    <message>
        <location filename="../net/peer.cpp" line="162"/>
        <source> is connected on P2P Network</source>
        <translation> está conectado a la red P2P</translation>
    </message>
    <message>
        <location filename="../net/peer.cpp" line="191"/>
        <source> leave from P2P Network</source>
        <translation> saliendo de la red P2P</translation>
    </message>
</context>
<context>
    <name>CSCManager</name>
    <message>
        <location filename="../sim/scmanager.cpp" line="44"/>
        <source>FSX/Prepar3D Module loaded</source>
        <translation>Módulo FSX / Prepar3D cargado</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="53"/>
        <source>FSX/Prepar3D Module unloaded</source>
        <translation>Módulo FSX / Prepar3D descargado</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="66"/>
        <source>Connexion to FS failed.</source>
        <translation>La conexión a FS falló.</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="81"/>
        <source>Disconnected from FS.</source>
        <translation>Desconectado de FS.</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="364"/>
        <source>Connected to simulator.</source>
        <translation>Conectado al simulador.</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="370"/>
        <source>Connected to FS.</source>
        <translation>Conectado a FS.</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="433"/>
        <source>Get a SimConnect exception = </source>
        <translation>Obteniendo una excepción SimConnect = </translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="434"/>
        <source> Related method invoked = </source>
        <translation> Método relacionado invocado = </translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="693"/>
        <source> with n°</source>
        <translation> con n°</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="742"/>
        <source>Receive SIMCONNECT_RECV_ASSIGNED_OBJECT_ID with unknown request ID  = </source>
        <translation>Reciba SIMCONNECT_RECV_ASSIGNED_OBJECT_ID con un ID de solicitud desconocido = </translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="773"/>
        <source>Send Text Failed</source>
        <translation>Error de envío de texto</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="791"/>
        <source> ; Tail = </source>
        <translation> ; Cola = </translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="844"/>
        <source>Delete Aircrfaft failed</source>
        <translation>La supresión de la aeronave falló</translation>
    </message>
</context>
<context>
    <name>CWebManager</name>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="81"/>
        <source>FFS2Play is connected on server</source>
        <translation>FFS2Play está conectado al servidor</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="134"/>
        <source>Connexion to server failed.</source>
        <translation>La conexión al servidor falló.</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="268"/>
        <source>This version of FFS2Play is legacy and does not suppported. Do you want update it ?</source>
        <translation>Esta versión de FFS2Play es heredada y no se admite. ¿Quieres actualizarla?</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="274"/>
        <source>This version of FFS2Play is not up to date. Do you want update it?</source>
        <translation>Esta versión de FFS2Play no está actualizada. ¿Quieres actualizarla?</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="280"/>
        <source>FFS2Play Update</source>
        <translation>Actualización de FFS2Play</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="324"/>
        <source>The X-Plane XFFS2Play plugin is not up to date. Do you want update it?</source>
        <translation>El plugin X-Plane XFFS2Play no está actualizado. ¿Quieres actualizarlo?</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="326"/>
        <source>XFFS2Play Plugin Update</source>
        <translation>Actualización del plugin XFFS2Play</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Requests.cpp" line="42"/>
        <source>Connecting to server ...</source>
        <translation>Conectando al servidor ...</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Requests.cpp" line="58"/>
        <source>Disconnecting from server ...</source>
        <translation>Desconectando del servidor ...</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Utils.cpp" line="97"/>
        <source>Server : </source>
        <translation>Servidor : </translation>
    </message>
    <message>
        <location filename="../web/webmanager.cpp" line="63"/>
        <source>Users Profils Settings is empty but default user is set. Reset of default user</source>
        <translation>La configuración del perfil del usuario está vacía, pero el usuario predeterminado está configurado. Reiniciando el perfil de usuario predeterminado</translation>
    </message>
    <message>
        <location filename="../web/webmanager.cpp" line="80"/>
        <source>Users Profils Settings checkum error, repair setting datas</source>
        <translation>Error de suma de comprobación de configuración de perfiles de usuarios, reparando datos de configuración</translation>
    </message>
</context>
<context>
    <name>CXPManager</name>
    <message>
        <location filename="../sim/xpmanager.cpp" line="60"/>
        <source>XPlane Module loaded</source>
        <translation>Módulo XPlane cargado</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="70"/>
        <source>XPlane Module unloaded</source>
        <translation>Módulo XPlane no se ha cargado</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="81"/>
        <source>Connecting to XPlane ...</source>
        <translation>Conectando a XPlane ...</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="98"/>
        <source>Disconnected from XPlane.</source>
        <translation>Desconectado de XPlane.</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="334"/>
        <source>Connected to XPlane.</source>
        <translation>Conectado a XPlane.</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="373"/>
        <source>DataSend Error : </source>
        <translation>Error de envío de datos : </translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="393"/>
        <source>Receive ADD_PEER event, failed to create object</source>
        <translation>Recibe el evento ADD_PEER, no se pudo crear el objeto</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="408"/>
        <source>ADD_PEER Error : </source>
        <translation>Error ADD_PEER : </translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="428"/>
        <source>DEL_PEER Error : </source>
        <translation>Error de DEL_PEER : </translation>
    </message>
</context>
<context>
    <name>DialogP2P</name>
    <message>
        <location filename="../gui/dialogp2p.ui" line="14"/>
        <source>P2P Settings</source>
        <translation>Configuraciones P2P</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="245"/>
        <source>uPNP NAT</source>
        <translation>uPNP NAT</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="254"/>
        <source>UPNP protocol, if your router enable it, give you the oportunity to open P2P UDP port automatically</source>
        <translation>El protocolo UPNP, si tiene accesi a su router y esta autorizado a hacer ajustes, este le dará la oportunidad de abrir el puerto P2P UDP automáticamente</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="282"/>
        <source>Enable NAT uPNP</source>
        <translation>Habilitar NAT uPNP</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="308"/>
        <source>Advanced Settings</source>
        <translation>Ajustes avanzados</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="316"/>
        <source>Transmit Rate</source>
        <translation>Tasa de transmisión</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="339"/>
        <source>mSec</source>
        <translation>mSeg</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="348"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;100 to 200 mSec : Fighters aircrafts&lt;br/&gt;200 to 300 mSec : Civil aircrafts&lt;br/&gt;400 msec : Liners&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt; &lt;p&gt; 100 a 200 mSec: Aviones de combate &lt;br/&gt; 200 a 300 mSec: Aviones civiles &lt;br/&gt; 400 msec: Liners &lt;/p&gt; &lt;/body&gt; &lt;/ html &gt;</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="26"/>
        <source>Multiplayer</source>
        <translation>Multijugador</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="235"/>
        <source>Enable TChat activity</source>
        <translation>Habilitar la actividad TChat</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="228"/>
        <source>Enable Shadow mode</source>
        <translation>Habilitar el modo Sombra</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="164"/>
        <source>AI Radius</source>
        <translation>Radio AI</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="184"/>
        <source>NM</source>
        <translation>Millas Nauticas</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="191"/>
        <source>AI Limit</source>
        <translation>Límite de IA</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="207"/>
        <source>Limit the number of AI. 0 mean unlimited</source>
        <translation>Limite el número de IA. 0 significa ilimitado</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="34"/>
        <source>Enable displaying Callsign over aircrafts</source>
        <translation>Habilitar la visualización de indicativos en aviones</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="37"/>
        <source>Enable Display CallSign</source>
        <translation>Habilitar Display del CallSign</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="387"/>
        <source>P2P Port</source>
        <translation>Puerto P2P</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="393"/>
        <source>P2P Port is used by FFS2Play to each other peers. Valid range for P2P application is 49152 to 65535</source>
        <translation>El puerto P2P es utilizado por FFS2Play entre sí. El rango válido para la aplicación P2P es de 49152 a 65535</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="414"/>
        <source>Port : </source>
        <translation>Puerto : </translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="455"/>
        <source>Enable TCP Client</source>
        <translation>Habilitar TCP</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="660"/>
        <source>XPlane AI Mode:</source>
        <translation>XPlane AI Modo:</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="667"/>
        <source>AI Aircrafts</source>
        <translation>Aviones AI</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="674"/>
        <source>CSL Aircrafts</source>
        <translation>Aviones CSL</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="361"/>
        <source>Beta Participation</source>
        <translation>Participación a las Beta</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="367"/>
        <source>Check the box below to receive Beta new version notification. Beta version cannot ensure stability and bugless</source>
        <translation>Marque la casilla de abajo para recibir notificaciones de nuevas versiones Beta. Una versión beta puede no ser estable y pude tener bugs</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="377"/>
        <source>Enable Beta Notification and installation</source>
        <translation>Habilitar notificación e instalación de nuevas Betas</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="473"/>
        <source>AI Folders</source>
        <translation>Carpeta de AI</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="524"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="552"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="577"/>
        <source>Reset Database</source>
        <translation>Reiniciar la base de datos</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="583"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="596"/>
        <source>Add here folders witch contain aircraft models on corresponding simulator. Exemple : the IVAO MTL library</source>
        <translation>Agregue aquí las carpetas que contienen modelos de aviones en el simulador correspondiente. Ejemplo: la biblioteca de IVAO MTL</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="609"/>
        <source>Miscelaneous</source>
        <translation>Diversos</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="636"/>
        <source>Display Touch Vertical Speed</source>
        <translation>Pantalla táctil de velocidad vertical</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="623"/>
        <source>Publish Touch Vertical Speed</source>
        <translation>Mostrar velocidad vertical táctil</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="646"/>
        <source>Disable Altitude Correction</source>
        <translation>Deshabilitar la corrección de altitud</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.cpp" line="487"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.cpp" line="488"/>
        <source>This function reset all AI Database for all simulator, are you sure to continue?</source>
        <translation>Esta función restablece todas las bases de datos de AI para todos los simuladores, ¿está seguro de continuar?</translation>
    </message>
</context>
<context>
    <name>DialogProfils</name>
    <message>
        <location filename="../gui/dialogprofils.ui" line="20"/>
        <source>Users Profils</source>
        <translation>Perfiles de usuarios</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="38"/>
        <source>Parameters</source>
        <translation>Parámetros</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="67"/>
        <source>Profil Name:</source>
        <translation>Nombre del perfil:</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="74"/>
        <source>Login:</source>
        <translation>Iniciar sesión:</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="81"/>
        <source>Password:</source>
        <translation>Contraseña:</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="101"/>
        <source>URL:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="158"/>
        <source>Select a profil:</source>
        <translation>Seleccione un perfil:</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="164"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="171"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.cpp" line="145"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.cpp" line="146"/>
        <source>There is some modifications on selected profil. Do you want apply them?</source>
        <translation>Hay algunas modificaciones en el perfil seleccionado. ¿Quieres aplicar los cambios?</translation>
    </message>
</context>
<context>
    <name>IniParser</name>
    <message>
        <location filename="../tools/iniparser.cpp" line="96"/>
        <source>Unable to open </source>
        <translation>No se puede abrir </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gui/mainwindow.ui" line="20"/>
        <source>FFS2Play</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="43"/>
        <source>Peer List</source>
        <translation>Lista de pares</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="74"/>
        <source>Mode</source>
        <translation>Modo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="79"/>
        <source>Remote Aircraft Name</source>
        <translation>Nombre de la aeronave remota</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="84"/>
        <source>ICAO Model</source>
        <translation>ICAO Modelo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="89"/>
        <location filename="../gui/mainwindow.ui" line="325"/>
        <source>Altitude</source>
        <translation>Altitud</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="94"/>
        <location filename="../gui/mainwindow.ui" line="340"/>
        <source>Heading</source>
        <translation>Rumbo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="99"/>
        <source>Distance</source>
        <translation>Distancia</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="104"/>
        <source>Air Speed(IAS)</source>
        <translation>Velocidad indicada(IAS)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="109"/>
        <source>Ground Speed(GS)</source>
        <translation>Velocidad sobre el suelo (GS)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="114"/>
        <source>Peer IP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="119"/>
        <source>Peer Port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="124"/>
        <source>Ping</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="129"/>
        <source>RcvCnt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="134"/>
        <source>SndCnt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="139"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="144"/>
        <location filename="../gui/mainwindow.ui" line="149"/>
        <location filename="../gui/mainwindow.ui" line="570"/>
        <location filename="../gui/mainwindow.ui" line="1100"/>
        <source>None</source>
        <translation>Ninguna</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="154"/>
        <source>ZZZZ</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="159"/>
        <source>0 ft</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="164"/>
        <source>0 °</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="169"/>
        <source>0 NM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="174"/>
        <location filename="../gui/mainwindow.ui" line="179"/>
        <source>0 Kts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="184"/>
        <source>255.X.X.255</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="189"/>
        <source>57435</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="194"/>
        <source>50 msec</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="199"/>
        <location filename="../gui/mainwindow.ui" line="204"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="216"/>
        <source>Tool Bar</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="245"/>
        <source>Tchat Zone</source>
        <translation>Área de discusión</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="283"/>
        <source>SIM Variables</source>
        <translation>Variables del simulador</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="300"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="305"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="310"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="315"/>
        <source>Category</source>
        <translation>Categoría</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="320"/>
        <source>Phase</source>
        <translation>Fase</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="330"/>
        <source>Ground Altitude</source>
        <translation>Altitud del suelo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="335"/>
        <source>Vertical Speed</source>
        <translation>Velocidad vertical</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="345"/>
        <source>Longitude</source>
        <translation>Longitud</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="350"/>
        <source>Latitude</source>
        <translation>Latitud</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="355"/>
        <source>Ground Speed</source>
        <translation>Velocidad sobre el suelo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="360"/>
        <source>TAS</source>
        <translation>velocidad verdadera (TAS)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="365"/>
        <source>Total Fuel Qty</source>
        <translation>Cantidad total de combustible</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="370"/>
        <source>Total Fuel Capacity</source>
        <translation>Capacidad total de combustible</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="375"/>
        <source>Fuel Weight per Gallon</source>
        <translation>Peso del combustible por galón</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="380"/>
        <source>Pitch</source>
        <translation>Cabeceo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="385"/>
        <source>Bank</source>
        <translation>Banco</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="390"/>
        <source>G Force</source>
        <translation>Fuerza G</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="395"/>
        <source>Total Weight</source>
        <translation>Peso total</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="400"/>
        <source>Realism</source>
        <translation>Realismo</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="405"/>
        <source>Ambient Wind Velocity</source>
        <translation>Velocidad del viento</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="410"/>
        <source>Ambient Wind Direction</source>
        <translation>Dirección del viento</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="415"/>
        <source>Ambiant Precipitation State</source>
        <translation>Estado de precipitaciones</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="420"/>
        <source>Sea level Pressure</source>
        <translation>Presión a nivel del mar</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="425"/>
        <source>IAS</source>
        <translation>Velocidad indicada (IAS)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="430"/>
        <source>Elevator Position</source>
        <translation>Posición del elevador</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="435"/>
        <source>Aileron Position</source>
        <translation>Posición del alerón</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="440"/>
        <source>Rudder Position</source>
        <translation>Posición del timón</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="445"/>
        <source>Simulation Rate</source>
        <translation>Tasa de simulación</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="450"/>
        <source>Exit 0</source>
        <translation>Salida 0</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="455"/>
        <source>Exit 1</source>
        <translation>Salida 1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="460"/>
        <source>Exit 2</source>
        <translation>Salida 2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="465"/>
        <source>Exit 3</source>
        <translation>Salida 3</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="470"/>
        <source>Parking Brake</source>
        <translation>Freno de mano</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="475"/>
        <source>Throttle 0</source>
        <translation>Acelerador 0</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="480"/>
        <source>Throttle 1</source>
        <translation>Mando de gas 1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="485"/>
        <source>Throttle 2</source>
        <translation>mando de gas 2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="490"/>
        <source>Throttle 3</source>
        <translation>mando de gas 3</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="495"/>
        <source>Beacon Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="500"/>
        <source>Landing Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="505"/>
        <source>Strobe Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="510"/>
        <source>Nav Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="515"/>
        <source>Reco Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="520"/>
        <source>State Engine 1</source>
        <translatorcomment>Estado del motor 1</translatorcomment>
        <translation>Motor de estado 1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="525"/>
        <source>State Engine 2</source>
        <translation>Estado del motor 2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="530"/>
        <source>State Engine 3</source>
        <translation>Estado del motor 3</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="535"/>
        <source>State Engine 4</source>
        <translation>Estado del motor 4</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="540"/>
        <source>Flaps Index</source>
        <translation>Indice de Flaps</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="545"/>
        <source>Squawk Mode</source>
        <translation>Modo transpondedor</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="550"/>
        <source>Squawk Code</source>
        <translation>Código de transpondedor</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="555"/>
        <source>Data</source>
        <translation>Datos</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="932"/>
        <source>Log</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1084"/>
        <source>Debug Options</source>
        <translation>Opciones de Debug</translation>
    </message>
    <message>
        <source>De&amp;bug Options</source>
        <translation type="vanished">Opciones de De&amp;bug</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1107"/>
        <source>Normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1114"/>
        <source>Verbose</source>
        <translation>Verboso</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1125"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1130"/>
        <source>AIMapping*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1135"/>
        <source>Analyzer*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1140"/>
        <source>HttpRequest*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1145"/>
        <source>P2PManager*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1150"/>
        <source>Peer*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1155"/>
        <source>UDPServer*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1160"/>
        <source>TCPServer*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1165"/>
        <source>TCPClient*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1170"/>
        <source>SCManager*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1175"/>
        <source>XPManager*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1180"/>
        <source>FGManager*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1185"/>
        <source>WEBManager*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1200"/>
        <location filename="../gui/mainwindow.ui" line="1203"/>
        <source>Connect to Sim</source>
        <translation>Conectarse a sim</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1213"/>
        <location filename="../gui/mainwindow.ui" line="1216"/>
        <source>Connect to web server</source>
        <extracomment>Connexion au serveur</extracomment>
        <translatorcomment>Conexión al servidor</translatorcomment>
        <translation>Conectar al servidor web</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1225"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1234"/>
        <source>Profil</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1243"/>
        <location filename="../gui/mainwindow.ui" line="1246"/>
        <source>P2P Settings</source>
        <translation>Configuraciones P2P</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1255"/>
        <source>Sim Type</source>
        <translation>Tipo sim</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1258"/>
        <source>Choose Sim to use</source>
        <translation>Elige Sim a utilizar</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.GuiEvents.cpp" line="82"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.GuiEvents.cpp" line="263"/>
        <source>Clear</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.GuiEvents.cpp" line="264"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.GuiEvents.cpp" line="286"/>
        <source>Web File (*.htm *.html)</source>
        <translation>Archivo web (* .htm * .html)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.GuiEvents.cpp" line="296"/>
        <source>An Error occur while opening </source>
        <translation>Ocurre un error al abrir </translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="90"/>
        <source>Welcome to FFS2Play</source>
        <translation>Bienvenido a FFS2Play</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="240"/>
        <source>Simulator Aircraft Folder</source>
        <translation>Carpeta de aviones simulador</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="240"/>
        <source>at least one folder is not configured for your simulator. Do you want add the path location now?</source>
        <translation>Al menos una carpeta no está configurada para su simulador. ¿Quieres añadir la ubicación de la ruta ahora?</translation>
    </message>
</context>
<context>
    <name>SCAIParser</name>
    <message>
        <location filename="../disk/scaiparser.cpp" line="168"/>
        <source>AIMapping : IniParser error = </source>
        <translation>AIMapping: IniParser error = </translation>
    </message>
    <message>
        <location filename="../disk/scaiparser.cpp" line="172"/>
        <source>AIMapping : Error = </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>XPAIParser</name>
    <message>
        <location filename="../disk/xpaiparser.cpp" line="214"/>
        <source>AIMapping : IniParser error = </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../disk/xpaiparser.cpp" line="218"/>
        <source>AIMapping : Error = </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>XPCSLParser</name>
    <message>
        <location filename="../disk/xpcslparser.cpp" line="126"/>
        <source>AIMapping : Error = </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../disk/xpcslparser.cpp" line="178"/>
        <source>AIMapping : Fail to open file = </source>
        <translation>AIMapping: Fallo al abrir el archivo = </translation>
    </message>
</context>
<context>
    <name>simbasemanager</name>
    <message>
        <location filename="../sim/simbasemanager.cpp" line="439"/>
        <source>Disconnected from simulator.</source>
        <translation>Desconectado del simulador.</translation>
    </message>
    <message>
        <location filename="../sim/simbasemanager.cpp" line="448"/>
        <source>Connexion to simulator timed out.</source>
        <translation>Se ha agotado el tiempo de conexión al simulador.</translation>
    </message>
</context>
</TS>
