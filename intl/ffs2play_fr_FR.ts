<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en">
<context>
    <name>AIBaseParser</name>
    <message>
        <location filename="../disk/aibaseparser.cpp" line="225"/>
        <source>Start updating aircraft database...</source>
        <translation>Début de la mise à jour de la base de donée des avions...</translation>
    </message>
    <message>
        <location filename="../disk/aibaseparser.cpp" line="258"/>
        <source>End of update : </source>
        <translation>Fin de la mise à jour : </translation>
    </message>
    <message>
        <location filename="../disk/aibaseparser.cpp" line="259"/>
        <source> aircrafts added, </source>
        <translation> appareil ajouté, </translation>
    </message>
    <message>
        <location filename="../disk/aibaseparser.cpp" line="260"/>
        <source> updated and </source>
        <translation> mis à jour et </translation>
    </message>
    <message>
        <location filename="../disk/aibaseparser.cpp" line="260"/>
        <source> deleted.</source>
        <translation> supprimés.</translation>
    </message>
</context>
<context>
    <name>AIReplace</name>
    <message>
        <location filename="../gui/aireplace.ui" line="14"/>
        <source>Aircraft Replacement</source>
        <translation>Remplacement d&apos;AI</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="34"/>
        <source>Origin Aircraft</source>
        <translation>Appareil d&apos;origine</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="65"/>
        <source>Replacement Aircraft</source>
        <translation>Appareil de Remplacement</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="95"/>
        <source>Filters</source>
        <translation>Filtres</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="107"/>
        <source>Editor :</source>
        <translation>Éditeur :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="133"/>
        <source>Number of engines :</source>
        <translation>Nombre de moteurs :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="146"/>
        <source>Model :</source>
        <translation>Modèle :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="159"/>
        <source>Engine type :</source>
        <translation>Type de moteur :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="172"/>
        <source>Category :</source>
        <translation>Catégorie :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="195"/>
        <source>Type :</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="277"/>
        <source>Rules</source>
        <translation>Règles</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="296"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="301"/>
        <source>Replacement</source>
        <translation>Remplacement</translation>
    </message>
    <message>
        <location filename="../gui/aireplace.ui" line="314"/>
        <source>Delete Rule</source>
        <translation>Suprimer la règle</translation>
    </message>
</context>
<context>
    <name>CFGManager</name>
    <message>
        <location filename="../sim/fgmanager.cpp" line="51"/>
        <source>FlightGear Module loaded</source>
        <translation>Module FlightGear chargé</translation>
    </message>
    <message>
        <location filename="../sim/fgmanager.cpp" line="61"/>
        <source>FlightGear Module unloaded</source>
        <translation>Module FlightGear déchargé</translation>
    </message>
    <message>
        <location filename="../sim/fgmanager.cpp" line="71"/>
        <source>Connecting to FlightGear ...</source>
        <translation>Connexion à FlightGear ...</translation>
    </message>
    <message>
        <location filename="../sim/fgmanager.cpp" line="77"/>
        <source>Connected to FlightGear.</source>
        <translation>Connecté à FlightGear.</translation>
    </message>
    <message>
        <location filename="../sim/fgmanager.cpp" line="94"/>
        <source>Disconnected from FlightGear.</source>
        <translation>Déconnecté de FlightGear.</translation>
    </message>
</context>
<context>
    <name>CP2PManager</name>
    <message>
        <location filename="../net/p2pmanager.cpp" line="273"/>
        <source>Adding UDP port mapping failed with code </source>
        <translation>L&apos;ajout du mapping du port UDP a échoué avec le code </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="288"/>
        <source>Adding TCP port mappping failed with code </source>
        <translation>L&apos;ajout du mapping du port TCP a échoué avec le code </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="293"/>
        <source>No valid Internet Gateway Device found.</source>
        <translation>Aucune passerelle Internet valide trouvée.</translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="299"/>
        <source>No IGD UPNP Device found on the network !</source>
        <translation>Aucun dispositif IGD UPNP trouvé sur le réseau !</translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="314"/>
        <source>Delete UDP port mapping failed with code </source>
        <translation>La suppression du mapping du port UDP a échoué avec le code </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="324"/>
        <source>Delete TCP port mapping failed with code </source>
        <translation>La suppression du mapping du port TCP a échoué avec le code </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="422"/>
        <location filename="../net/p2pmanager.cpp" line="464"/>
        <source>Bad IP Address</source>
        <translation>Mauvaise adresse IP</translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="450"/>
        <source>User </source>
        <translation>Utilisateur </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="837"/>
        <source>Drop rate : </source>
        <translation>Taux de chute : </translation>
    </message>
    <message>
        <location filename="../net/p2pmanager.cpp" line="837"/>
        <source> ft/min</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CPeer</name>
    <message>
        <location filename="../net/peer.cpp" line="162"/>
        <source> is connected on P2P Network</source>
        <translation> est connecté sur le réseau P2P</translation>
    </message>
    <message>
        <location filename="../net/peer.cpp" line="191"/>
        <source> leave from P2P Network</source>
        <translation> quitte le réseau P2P</translation>
    </message>
</context>
<context>
    <name>CSCManager</name>
    <message>
        <location filename="../sim/scmanager.cpp" line="44"/>
        <source>FSX/Prepar3D Module loaded</source>
        <translation>Module FSX/Prepar3D chargé</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="53"/>
        <source>FSX/Prepar3D Module unloaded</source>
        <translation>Module FSX/Prepar3D déchargé</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="66"/>
        <source>Connexion to FS failed.</source>
        <translation>La connexion au simulateur a échoué.</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="81"/>
        <source>Disconnected from FS.</source>
        <translation>Déconnecté du simulateur.</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="364"/>
        <source>Connected to simulator.</source>
        <translation>Connecté au simulateur.</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="370"/>
        <source>Connected to FS.</source>
        <translation>Connecté au simulateur.</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="433"/>
        <source>Get a SimConnect exception = </source>
        <translation>Reçu une exception SimConnect = </translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="434"/>
        <source> Related method invoked = </source>
        <translation> Methode en relation appelée = </translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="693"/>
        <source> with n°</source>
        <translation> avec le n°</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="742"/>
        <source>Receive SIMCONNECT_RECV_ASSIGNED_OBJECT_ID with unknown request ID  = </source>
        <translation>Reçu un SIMCONNECT_RECV_ASSIGNED_OBJECT_ID avec un request ID inconnu = </translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="773"/>
        <source>Send Text Failed</source>
        <translation>L&apos;envoi du text a échoué</translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="791"/>
        <source> ; Tail = </source>
        <translation> ; Tail = </translation>
    </message>
    <message>
        <location filename="../sim/scmanager.cpp" line="844"/>
        <source>Delete Aircrfaft failed</source>
        <translation>Supression de l&apos;appareil a échouée</translation>
    </message>
</context>
<context>
    <name>CWebManager</name>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="81"/>
        <source>FFS2Play is connected on server</source>
        <translation>FFS2Play est connecté au serveur</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="134"/>
        <source>Connexion to server failed.</source>
        <translation>La connexion au server a échouée.</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="268"/>
        <source>This version of FFS2Play is legacy and does not suppported. Do you want update it ?</source>
        <translation>Cette version de FFS2Play est obsolète et n&apos;est plus supporté. Désirez vous la mettre à jour?</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="274"/>
        <source>This version of FFS2Play is not up to date. Do you want update it?</source>
        <translation>Cette version de FFS2Play n&apos;est pas à jour. Souhaitez vous la mettre à jour?</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="280"/>
        <source>FFS2Play Update</source>
        <translation>Mise jour FFS2Play</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="324"/>
        <source>The X-Plane XFFS2Play plugin is not up to date. Do you want update it?</source>
        <translation>Le Plugin XFFS2Play est périmé. Souhaitez vous le mettre à jour?</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Callback.cpp" line="326"/>
        <source>XFFS2Play Plugin Update</source>
        <translation>Mise à jour du plugin XFFS2Play</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Utils.cpp" line="97"/>
        <source>Server : </source>
        <translation>Serveur : </translation>
    </message>
    <message>
        <location filename="../web/webmanager.cpp" line="63"/>
        <source>Users Profils Settings is empty but default user is set. Reset of default user</source>
        <translation>La liste des profils utilisateur est vide pourtant un utilisateur par défaut est spécifié. Remise à zéro de l&apos;utilisateur par défaut</translation>
    </message>
    <message>
        <location filename="../web/webmanager.cpp" line="80"/>
        <source>Users Profils Settings checkum error, repair setting datas</source>
        <translation>La liste des profils d&apos;utilisateur présente une erreur de checksum, réparation des donnée</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Requests.cpp" line="42"/>
        <source>Connecting to server ...</source>
        <translation>Connexion au serveur ...</translation>
    </message>
    <message>
        <location filename="../web/webmanager.Requests.cpp" line="58"/>
        <source>Disconnecting from server ...</source>
        <translation>Déconnexion du serveur ...</translation>
    </message>
</context>
<context>
    <name>CXPManager</name>
    <message>
        <location filename="../sim/xpmanager.cpp" line="60"/>
        <source>XPlane Module loaded</source>
        <translation>Le module XPlane a été chargé</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="70"/>
        <source>XPlane Module unloaded</source>
        <translation>Le module XPlane a été déchargé</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="81"/>
        <source>Connecting to XPlane ...</source>
        <translation>Connexion à X-Plane ...</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="98"/>
        <source>Disconnected from XPlane.</source>
        <translation>Déconnexion de X-Plane.</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="334"/>
        <source>Connected to XPlane.</source>
        <translation>Connecté à X-Plane.</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="373"/>
        <source>DataSend Error : </source>
        <translation>Erreur DataSend : </translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="393"/>
        <source>Receive ADD_PEER event, failed to create object</source>
        <translation>Réception d&apos; un événement ADD_PEER, impossible de créer l&apos;objet</translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="408"/>
        <source>ADD_PEER Error : </source>
        <translation>Erreur ADD_PEER : </translation>
    </message>
    <message>
        <location filename="../sim/xpmanager.cpp" line="428"/>
        <source>DEL_PEER Error : </source>
        <translation>Erreur DEL_PEER : </translation>
    </message>
</context>
<context>
    <name>DialogP2P</name>
    <message>
        <location filename="../gui/dialogp2p.ui" line="14"/>
        <source>P2P Settings</source>
        <translation>Paramètres P2P</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="308"/>
        <source>Advanced Settings</source>
        <translation>Paramètres avancés</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="339"/>
        <source>mSec</source>
        <translation>mSec</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="26"/>
        <source>Multiplayer</source>
        <translation>Multijoueur</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="164"/>
        <source>AI Radius</source>
        <translation>Rayon AI</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="184"/>
        <source>NM</source>
        <translation>NM</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="191"/>
        <source>AI Limit</source>
        <translation>Limite AI</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="245"/>
        <source>uPNP NAT</source>
        <translation>uPNP NAT</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="254"/>
        <source>UPNP protocol, if your router enable it, give you the oportunity to open P2P UDP port automatically</source>
        <translation>Le protocole uPNP, si votre routeur l&apos;autorise, vous donne la possibilité d&apos;ouvrir les ports UDP P2P automatiquement</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="282"/>
        <source>Enable NAT uPNP</source>
        <translation>Autoriser le NAT uPNP</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="316"/>
        <source>Transmit Rate</source>
        <translation>Taux de transmission</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="348"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;100 to 200 mSec : Fighters aircrafts&lt;br/&gt;200 to 300 mSec : Civil aircrafts&lt;br/&gt;400 msec : Liners&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;100 à 200 mSec : Avions de combat&lt;br/&gt;200 à 300 mSec : Avions de tourisme&lt;br/&gt;400 msec : Avions de ligne&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="235"/>
        <source>Enable TChat activity</source>
        <translation>Autorise l&apos;activité du TChat</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="228"/>
        <source>Enable Shadow mode</source>
        <translation>Autoriser le mode ombre</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="207"/>
        <source>Limit the number of AI. 0 mean unlimited</source>
        <translation>Limite du nombre d&apos;AI. 0 signifie illimité</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="34"/>
        <source>Enable displaying Callsign over aircrafts</source>
        <translation>Autorise l&apos;affichage du pseudo</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="37"/>
        <source>Enable Display CallSign</source>
        <translation>Autoriser l&apos;affichage du pseudo</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="387"/>
        <source>P2P Port</source>
        <translation>Port P2P</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="393"/>
        <source>P2P Port is used by FFS2Play to each other peers. Valid range for P2P application is 49152 to 65535</source>
        <translation>Le port P2P est utilisé par FFS2Plau pour joindre les autres joueurs. La plage valide pour une application P2P est de 49152 à 65535</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="414"/>
        <source>Port : </source>
        <translation>Port : </translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="455"/>
        <source>Enable TCP Client</source>
        <translation>Autoriser Client TCP</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="361"/>
        <source>Beta Participation</source>
        <translation>Participation aux versions beta</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="367"/>
        <source>Check the box below to receive Beta new version notification. Beta version cannot ensure stability and bugless</source>
        <translation>Cochez la boite pour recevoir les notification de nouvelles versions Beta. Les version Beta ne peuvent pas garantir la stabilité</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="377"/>
        <source>Enable Beta Notification and installation</source>
        <translation>Autoriser les notifications et l&apos;installation des version betas</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="473"/>
        <source>AI Folders</source>
        <translation>Dossiers des AI</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="524"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="552"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="577"/>
        <source>Reset Database</source>
        <translation>Réinitialisation de la base de donnée</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="583"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="596"/>
        <source>Add here folders witch contain aircraft models on corresponding simulator. Exemple : the IVAO MTL library</source>
        <translation>Ajouter ici les répertoires qui contiennent les models d&apos;avions du simulateur correspondant. Example : la bibliothèque IVAO MTL</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="609"/>
        <source>Miscelaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="636"/>
        <source>Display Touch Vertical Speed</source>
        <translation>Afficher le taux de chute</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="660"/>
        <source>XPlane AI Mode:</source>
        <translation>Mode AI XPlane:</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="667"/>
        <source>AI Aircrafts</source>
        <translation>Appareils AI</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="674"/>
        <source>CSL Aircrafts</source>
        <translation>Appareils CSL</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="623"/>
        <source>Publish Touch Vertical Speed</source>
        <translation>Publier le taux de chute</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.ui" line="646"/>
        <source>Disable Altitude Correction</source>
        <translation>Désactive la correction automatique d&apos;altitude</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.cpp" line="487"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../gui/dialogp2p.cpp" line="488"/>
        <source>This function reset all AI Database for all simulator, are you sure to continue?</source>
        <translation>Cette fonction efface toute la base de donnée des AI, êtes vous sûre de vouloir continuer?</translation>
    </message>
</context>
<context>
    <name>DialogProfils</name>
    <message>
        <location filename="../gui/dialogprofils.ui" line="20"/>
        <source>Users Profils</source>
        <translation>Profils Utilisateurs</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="38"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="67"/>
        <source>Profil Name:</source>
        <translation>Nom du profil:</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="74"/>
        <source>Login:</source>
        <translation>Compte:</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="81"/>
        <source>Password:</source>
        <translation>Mot de Passe:</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="101"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="158"/>
        <source>Select a profil:</source>
        <translation>Sélectionnez un profil:</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="164"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.ui" line="171"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.cpp" line="145"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../gui/dialogprofils.cpp" line="146"/>
        <source>There is some modifications on selected profil. Do you want apply them?</source>
        <translation>Il y a des modifications en cours dans le profil sélectionné. souhaitez vous les appliquer?</translation>
    </message>
</context>
<context>
    <name>IniParser</name>
    <message>
        <location filename="../tools/iniparser.cpp" line="96"/>
        <source>Unable to open </source>
        <translation>Impossible d&apos;ouvrir </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gui/mainwindow.ui" line="20"/>
        <source>FFS2Play</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="216"/>
        <source>Tool Bar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="144"/>
        <location filename="../gui/mainwindow.ui" line="149"/>
        <location filename="../gui/mainwindow.ui" line="570"/>
        <location filename="../gui/mainwindow.ui" line="1100"/>
        <source>None</source>
        <translation>Aucune</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="43"/>
        <source>Peer List</source>
        <translation>Liste des joueurs</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="74"/>
        <source>Mode</source>
        <translation></translation>
    </message>
    <message>
        <source>De&amp;bug Options</source>
        <translation type="vanished">Options De&amp;bug</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1107"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1114"/>
        <source>Verbose</source>
        <translation>Bavard</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1125"/>
        <source>*</source>
        <translation>*</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1130"/>
        <source>AIMapping*</source>
        <translation>AIMapping*</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1140"/>
        <source>HttpRequest*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1145"/>
        <source>P2PManager*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1150"/>
        <source>Peer*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1155"/>
        <source>UDPServer*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1160"/>
        <source>TCPServer*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1165"/>
        <source>TCPClient*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1170"/>
        <source>SCManager*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1175"/>
        <source>XPManager*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1185"/>
        <source>WEBManager*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="300"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="245"/>
        <source>Tchat Zone</source>
        <translation>Zone de discussion</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="283"/>
        <source>SIM Variables</source>
        <translation>Variables du Simulateur</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="305"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="310"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="315"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="320"/>
        <source>Phase</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="89"/>
        <location filename="../gui/mainwindow.ui" line="325"/>
        <source>Altitude</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="330"/>
        <source>Ground Altitude</source>
        <translation>Altitude du sol</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="335"/>
        <source>Vertical Speed</source>
        <translation>Vitesse verticale</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="94"/>
        <location filename="../gui/mainwindow.ui" line="340"/>
        <source>Heading</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="79"/>
        <source>Remote Aircraft Name</source>
        <translation>Nom de l&apos;appareil distant</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="84"/>
        <source>ICAO Model</source>
        <translation>Model OACI</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="99"/>
        <source>Distance</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="104"/>
        <source>Air Speed(IAS)</source>
        <translation>Vitesse Air(IAS)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="109"/>
        <source>Ground Speed(GS)</source>
        <translation>Vitesse Sol(GS)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="114"/>
        <source>Peer IP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="119"/>
        <source>Peer Port</source>
        <translation>Port du Peer</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="124"/>
        <source>Ping</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="129"/>
        <source>RcvCnt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="134"/>
        <source>SndCnt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="139"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="154"/>
        <source>ZZZZ</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="159"/>
        <source>0 ft</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="164"/>
        <source>0 °</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="169"/>
        <source>0 NM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="174"/>
        <location filename="../gui/mainwindow.ui" line="179"/>
        <source>0 Kts</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="184"/>
        <source>255.X.X.255</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="189"/>
        <source>57435</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="194"/>
        <source>50 msec</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="199"/>
        <location filename="../gui/mainwindow.ui" line="204"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;Tchat Zone</source>
        <translation type="vanished">Zone de &amp;Tchat</translation>
    </message>
    <message>
        <source>SIM &amp;Variables</source>
        <translation type="vanished">&amp;Variable du SIM</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="345"/>
        <source>Longitude</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="350"/>
        <source>Latitude</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="355"/>
        <source>Ground Speed</source>
        <translation>Vitesse Sol</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="360"/>
        <source>TAS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="365"/>
        <source>Total Fuel Qty</source>
        <translation>Quantité totale de carburant</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="370"/>
        <source>Total Fuel Capacity</source>
        <translation>Capacité totale de carburant</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="375"/>
        <source>Fuel Weight per Gallon</source>
        <translation>Poids du carburant au Gallon</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="380"/>
        <source>Pitch</source>
        <translation>Tangage</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="385"/>
        <source>Bank</source>
        <translation>Roulis</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="390"/>
        <source>G Force</source>
        <translation>Force G</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="395"/>
        <source>Total Weight</source>
        <translation>Poids Total</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="400"/>
        <source>Realism</source>
        <translation>Réalisme</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="405"/>
        <source>Ambient Wind Velocity</source>
        <translation>Vitesse du vent ambient</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="410"/>
        <source>Ambient Wind Direction</source>
        <translation>Direction du vent ambiant</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="415"/>
        <source>Ambiant Precipitation State</source>
        <translation>Etat de la précipitation ambiante</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="420"/>
        <source>Sea level Pressure</source>
        <translation>Pression au niveau de la mer</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="425"/>
        <source>IAS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="430"/>
        <source>Elevator Position</source>
        <translation>Position profondeur</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="435"/>
        <source>Aileron Position</source>
        <translation>Position des ailerons</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="440"/>
        <source>Rudder Position</source>
        <translation>Position de la dérive</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="445"/>
        <source>Simulation Rate</source>
        <translation>Vitesse de simulation</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="450"/>
        <source>Exit 0</source>
        <translation>Sortie 0</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="455"/>
        <source>Exit 1</source>
        <translation>Sortie 1</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="460"/>
        <source>Exit 2</source>
        <translation>Sortie 2</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="465"/>
        <source>Exit 3</source>
        <translation>Sortie 3</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="470"/>
        <source>Parking Brake</source>
        <translation>Frein de parking</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="475"/>
        <source>Throttle 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="480"/>
        <source>Throttle 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="485"/>
        <source>Throttle 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="490"/>
        <source>Throttle 3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="495"/>
        <source>Beacon Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="500"/>
        <source>Landing Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="505"/>
        <source>Strobe Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="510"/>
        <source>Nav Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="515"/>
        <source>Reco Light</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="520"/>
        <source>State Engine 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="525"/>
        <source>State Engine 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="530"/>
        <source>State Engine 3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="535"/>
        <source>State Engine 4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="540"/>
        <source>Flaps Index</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="545"/>
        <source>Squawk Mode</source>
        <translation>Mode Transpondeur</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="550"/>
        <source>Squawk Code</source>
        <translation>Code Transpondeur</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="555"/>
        <source>Data</source>
        <translation>Données</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="932"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1084"/>
        <source>Debug Options</source>
        <translation>Options de débogage</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1135"/>
        <source>Analyzer*</source>
        <translation>Analyzer*</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1180"/>
        <source>FGManager*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1200"/>
        <location filename="../gui/mainwindow.ui" line="1203"/>
        <source>Connect to Sim</source>
        <translation>Connection au Simulateur</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1213"/>
        <location filename="../gui/mainwindow.ui" line="1216"/>
        <source>Connect to web server</source>
        <extracomment>Connexion au serveur</extracomment>
        <translation>Connection au serveur Web</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1225"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1243"/>
        <location filename="../gui/mainwindow.ui" line="1246"/>
        <source>P2P Settings</source>
        <translation>Paramètres P2P</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1255"/>
        <source>Sim Type</source>
        <translation>Type de simulateur</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1258"/>
        <source>Choose Sim to use</source>
        <translation>Choisissez le type de simulateur à utiliser</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.ui" line="1234"/>
        <source>Profil</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="90"/>
        <source>Welcome to FFS2Play</source>
        <translation>Bienvenue sur FFS2Play</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="240"/>
        <source>Simulator Aircraft Folder</source>
        <translation>Dossier des appareils du Simulateur</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.cpp" line="240"/>
        <source>at least one folder is not configured for your simulator. Do you want add the path location now?</source>
        <translation>au moins un dossier n&apos;est pas configuré pour votre simulateur. Voulez vous en ajouter un maintenant?</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.GuiEvents.cpp" line="82"/>
        <source>About</source>
        <translation>A Propos</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.GuiEvents.cpp" line="263"/>
        <source>Clear</source>
        <translation>Vider</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.GuiEvents.cpp" line="264"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.GuiEvents.cpp" line="286"/>
        <source>Web File (*.htm *.html)</source>
        <translation>Fichier Web (*.htm *.html)</translation>
    </message>
    <message>
        <location filename="../gui/mainwindow.GuiEvents.cpp" line="296"/>
        <source>An Error occur while opening </source>
        <translation>Une erreur est survenue lors de l&apos;ouverture de </translation>
    </message>
</context>
<context>
    <name>SCAIParser</name>
    <message>
        <location filename="../disk/scaiparser.cpp" line="168"/>
        <source>AIMapping : IniParser error = </source>
        <translation>AIMapping : erreur du IniParser = </translation>
    </message>
    <message>
        <location filename="../disk/scaiparser.cpp" line="172"/>
        <source>AIMapping : Error = </source>
        <translation>AIMapping : Erreur = </translation>
    </message>
</context>
<context>
    <name>XPAIParser</name>
    <message>
        <location filename="../disk/xpaiparser.cpp" line="214"/>
        <source>AIMapping : IniParser error = </source>
        <translation>AIMapping : erreur du IniParser = </translation>
    </message>
    <message>
        <location filename="../disk/xpaiparser.cpp" line="218"/>
        <source>AIMapping : Error = </source>
        <translation>AIMapping : Erreur = </translation>
    </message>
</context>
<context>
    <name>XPCSLParser</name>
    <message>
        <location filename="../disk/xpcslparser.cpp" line="126"/>
        <source>AIMapping : Error = </source>
        <translation>AIMapping : Erreur = </translation>
    </message>
    <message>
        <location filename="../disk/xpcslparser.cpp" line="178"/>
        <source>AIMapping : Fail to open file = </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>simbasemanager</name>
    <message>
        <location filename="../sim/simbasemanager.cpp" line="439"/>
        <source>Disconnected from simulator.</source>
        <translation>Déconnecté du simulateur.</translation>
    </message>
    <message>
        <location filename="../sim/simbasemanager.cpp" line="448"/>
        <source>Connexion to simulator timed out.</source>
        <translation>Connexion au simulateur expiré.</translation>
    </message>
</context>
</TS>
